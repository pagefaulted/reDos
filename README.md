reDos (REalDumbOS)
=====

This project consists of a real dumb operating system and its relative bootloader.

The language of choice is nasm real mode 16 bits assembly.

I use qemu for testing, by passing the compiled bootloader (rBoot) binary to the -fda option and the os binary as the disk image ( qemu-system-i386 -s -S -fda rboot.bin os.bin).

I use gdb for debugging, connecting to qemu executing "target remote localhost:1234" and setting the architecture
to i8086 with the command "set architecture i8086".
