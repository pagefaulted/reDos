[bits 16]
[ORG 0x7c00]
[CPU 586]

; This is the tipical layout of a FDISK MBR. rBoot does not need a partition table for now.
;  0x0 ---> 0x8a  Code.
; 0x8b ---> 0xbd  Messages.
; 0xbe ---> 0xfd  Partitions Table.
; 0xff ---> 0xfe  MBR signature.

; macros
%define os_destination_address 0x7c00
%define reloc_address          0x0600
%define line_feed              0x0a, 0x0d, 0x00
%define jump_reloc_addr        0x616
%define size_of_this_mbr       0x200
%define addr_error_code        0x06b1
%define addr_error_mess        0x06b3


MAIN:
  ; set ss to 0x00.
  xor ax, ax
  mov ss, ax

  ; set ds and es segment registers to 0x0.
  mov ds, ax
  mov es, ax

  mov si, welcome_mess
  call PRINT
  call RELOCATE
  mov ax, jump_reloc_addr
  jmp ax
  call LOAD_OS
  mov ax, os_destination_address
  jmp ax


; relocates this program @0x0600.
RELOCATE:
  ; reset the df flag: the string instruction will increment esi and edi.
  cld
  ; the size of this MBR.
  mov cx, size_of_this_mbr
  mov si, os_destination_address
  mov di, reloc_address
  rep movsb
  ret


; prints bytes at si, up to the first 0x00.
PRINT:
  ; reset the df flag.
  cld
  mov ah, 0x0E

  loop_print:
    ; load the byte pointed by the source operand si to al.
    lodsb
    test al, al
    jz exit
    int 0x10
    jmp loop_print
  exit:
    ret


; prints the hex byte pointed by si.
PRINT_HEX:
  cld
  mov ah, 0x0E

 loop_hex:
   lodsb
   test al, al
   jz exit
   mov cl, al
   mov bl, al
   call NIBBLE_TO_HEX
   mov al, bl
   int 0x10
   shl cl, 0x04
   mov bl, cl
   call NIBBLE_TO_HEX
   mov al, bl
   int 0x10
   jmp loop_hex

   exit_hex:
     ret


; changes the high nibble in bl into its hex representation.
NIBBLE_TO_HEX:
  shr bl, 0x04
  cmp bl, 0x09
  jle less_than_10
  ; >= 10, add ascii 'a' - 0xa
  add bl, 0x37
  ret

  less_than_10:
    ; < 10, add ascii '0'
    add bl, 0x30
    ret


LOAD_OS:
  mov ah, 0x02
  ; number of sectors to read
  mov al, 0x01
  ; low 8 bits of cilinder number
  mov ch, 0x00
  ; sector number
  mov cl, 0x01
  ; head number
  mov dh, 0x00
  ; drive number
  mov dl, 0x80
  mov bx, 0x0000
  mov es, bx
  mov bx, os_destination_address
  ; es:bx -> buffer
  int 0x13
  jc error
  ret

  error:
    push ax
    mov si, addr_error_mess
    call PRINT
    pop ax

    mov [addr_error_code], ah
    mov si, addr_error_code
    call PRINT_HEX
    cli
    hlt


welcome_mess db "Welcome to Rboot! :D", line_feed
error_code dw 0x0000
error_mess db "We are dead in the water. int13 returned "


times 510 - ($ - $$) db 0x00
dw 0xAA55
